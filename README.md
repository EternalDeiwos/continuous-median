# Continuous Median

[![standard-readme compliant](https://img.shields.io/badge/standard--readme-OK-green.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)

> An attempt to solve the [Coninuous Median](https://open.kattis.com/problems/continuousmedian) problem from NUS Competetive Programming.

## Table of Contents

- [Install](#install)
- [Usage](#usage)
- [Maintainers](#maintainers)
- [Contributing](#contributing)
- [License](#license)

## Install

Gradle will automatically initialize itself and download necessary dependencies the first time the wrapper is run. No explicit installation necessary.

## Usage

For Linux/macOS users, open a terminal and run:

```sh
./gradlew clean test
```

For Windows users, use `gradlew.bat` instead of `gradlew` in PowerShell.

## Maintainers

[Greg Linklater (@EternalDeiwos)](https://gitlab.com/EternalDeiwos)

## Contributing

PRs accepted.

Small note: If editing the README, please conform to the [standard-readme](https://github.com/RichardLitt/standard-readme) specification.

## License

MIT © 2022 Greg Linklater
