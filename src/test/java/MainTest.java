import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.*;
import java.util.Scanner;

class MainTest {
    static final String[] inputs = new String[] {
            "src/main/resources/sampleInput1.txt",
            "src/main/resources/generated1.txt",
            "src/main/resources/generated2.txt",
            "src/main/resources/generated3.txt",
            "src/main/resources/generated4.txt",
            "src/main/resources/generated5.txt",
            "src/main/resources/mainGenerated.txt"
    };
    static final String[] outputs = new String[] {
            "src/main/resources/sampleOutput1.txt",
            "src/main/resources/generated1Output.txt",
            "src/main/resources/generated2Output.txt",
            "src/main/resources/generated3Output.txt",
            "src/main/resources/generated4Output.txt",
            "src/main/resources/generated5Output.txt",
            "src/main/resources/mainGeneratedOutput.txt"
    };

    @Test
    void sample1() throws IOException {
        for (int i = 0; i < inputs.length; i++) {
            InputStream inputStream = new FileInputStream(inputs[i]);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            Main.start(inputStream, baos);
            Scanner expectedOutput = new Scanner(new BufferedReader(new InputStreamReader(new FileInputStream(outputs[i]))));
            Scanner actualOutput = new Scanner(new InputStreamReader(new ByteArrayInputStream(baos.toByteArray())));
            while (expectedOutput.hasNext()) {
                Assertions.assertEquals(expectedOutput.nextLine(), actualOutput.nextLine());
            }
        }
    }
}