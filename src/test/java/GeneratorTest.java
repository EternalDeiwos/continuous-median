import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class GeneratorTest {
    static final int TESTS = 4;
    static final int SIZE = 10000;
    static final int MASK = 0x05FFFFFF; // 10^9-ish
    InputStream outputStream;
    InputStream inputStream;

    @BeforeEach
    void setup() {
        NaiveGenerator generator = new NaiveGenerator(MASK);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        PrintWriter writer = new PrintWriter(new BufferedWriter(new OutputStreamWriter(baos)));
        ArrayList<String> outputs = new ArrayList();
        writer.printf("%d\n", TESTS);

        int count = TESTS;
        while(count-- > 0) {
            NaiveGenerator.Result result = generator.produce(SIZE);
            writer.printf("%d\n", result.input.length);
            for (int i = 0; i < result.input.length; i++) {
                writer.write(Integer.toString(result.input[i]));
                if (i < result.input.length-1) {
                    writer.write(' ');
                }
            }
            writer.write('\n');
            outputs.add(Long.toString(result.output));
        }

        writer.close();

        inputStream = new ByteArrayInputStream(baos.toByteArray());
        outputStream = new ByteArrayInputStream(String.join("\n", outputs).getBytes(StandardCharsets.UTF_8));
    }

    @Test
    void generator() throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        Main.start(inputStream, baos);
        Scanner expectedOutput = new Scanner(new BufferedReader(new InputStreamReader(outputStream)));
        Scanner actualOutput = new Scanner(new BufferedReader(new InputStreamReader(new ByteArrayInputStream(baos.toByteArray()))));
        while (actualOutput.hasNext()) {
            Assertions.assertEquals(expectedOutput.nextLine(), actualOutput.nextLine());
        }
    }
}
