import java.io.*;
import java.util.Arrays;
import java.util.StringTokenizer;

public class Main {
    // Constants
    static final int MAX_SIZE = 100000;
    static final char NO_BIAS = 0;
    static final char LEFT = 'l';
    static final char RIGHT = 'r';

    // AA Self-balancing Binary Search Tree
    static int[] value = new int[MAX_SIZE];
    static int[] left = new int[MAX_SIZE];
    static int[] right = new int[MAX_SIZE];
    static int[] level = new int[MAX_SIZE];
    static int[] parent = new int[MAX_SIZE];
    static int root;
    static int medianIndex;
    static int currentNodes;

    public static void main(String[] args) {
        try {
            if (args.length > 0) {
                FileInputStream fis = new FileInputStream(args[0]);
                start(fis, System.out);
                return;
            }

            start(System.in, System.out);
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    public static void start(InputStream in, OutputStream out) throws IOException {
        Kattio io = new Kattio(in, out);

        int T = io.getInt();

        // For each distinct test 0..T
        while (T-- > 0) {
            int N = io.getInt();
            long sumOfMedians = 0;

            // Reset state
            root = 0;
            medianIndex = 0;
            currentNodes = 0;
            Arrays.fill(value, -1);
            Arrays.fill(left, -1);
            Arrays.fill(right, -1);
            Arrays.fill(parent, -1);
            Arrays.fill(level, -1);

            char bias = NO_BIAS;

            // Run algorithm
            while ((N - currentNodes) > 0) {
                int currentValue = io.getInt();
                root = insert(currentValue, root);

                int medianValue = value[medianIndex];
                if (currentNodes % 2 != 0) {
                    // Traverse right
                    if (currentValue >= medianValue && bias == RIGHT) {
                        medianIndex = next(medianIndex);

                    // Traverse left
                    } else if (currentValue < medianValue && bias == LEFT) {
                        medianIndex = prev(medianIndex);
                    }

                    bias = NO_BIAS;
                    sumOfMedians += value[medianIndex];

                } else {
                    int otherIndex;

                    // Look right
                    if (currentValue >= medianValue) {
                        otherIndex = next(medianIndex);
                        bias = RIGHT;

                    // Look left
                    } else {
                        otherIndex = prev(medianIndex);
                        bias = LEFT;
                    }

                    sumOfMedians += (value[medianIndex] + value[otherIndex]) / 2;
                }
            }

            io.printf("%d\n", sumOfMedians);
        }

        io.close();
    }

    static int skew (int nodeIndex) {
        if (nodeIndex != -1 && left[nodeIndex] != -1 && level[left[nodeIndex]] == level[nodeIndex]) {
            int tempNode = left[nodeIndex];
            parent[tempNode] = parent[nodeIndex];
            left[nodeIndex] = right[tempNode];
            if (right[tempNode] != -1) {
                parent[right[tempNode]] = nodeIndex;
            }
            right[tempNode] = nodeIndex;
            parent[nodeIndex] = tempNode;

            return tempNode;
        }

        return nodeIndex;
    }

    static int split (int nodeIndex) {
        if (nodeIndex != -1 && right[nodeIndex] != -1 && right[right[nodeIndex]] != -1
                && level[nodeIndex] == level[right[right[nodeIndex]]]) {
            int tempNode = right[nodeIndex];
            parent[tempNode] = parent[nodeIndex];
            parent[nodeIndex] = tempNode;
            right[nodeIndex] = left[tempNode];
            if (left[tempNode] != -1) {
                parent[left[tempNode]] = nodeIndex;
            }
            left[tempNode] = nodeIndex;
            level[tempNode] = level[tempNode] + 1;

            return tempNode;
        }

        return nodeIndex;
    }

    static int insert (int currentValue, int rootIndex) {
        // New Node
        if (rootIndex == -1 || value[rootIndex] == -1) {
            int newNode = currentNodes++;
            level[newNode] = 1;
            value[newNode] = currentValue;
            return newNode;

        // Traverse left
        } else if (currentValue < value[rootIndex]) {
            int node = insert(currentValue, left[rootIndex]);
            left[rootIndex] = node;
            parent[node] = rootIndex;

        // Traverse right
        } else {
            int node = insert(currentValue, right[rootIndex]);
            right[rootIndex] = node;
            parent[node] = rootIndex;
        }

        // Maintain balance
        rootIndex = skew(rootIndex);
        rootIndex = split(rootIndex);

        return rootIndex;
    }

    static int next(int nodeIndex) {
        int targetIndex = right[nodeIndex];
        if (targetIndex != -1) {
            while (left[targetIndex] != -1) {
                targetIndex = left[targetIndex];
            }
            return targetIndex;

        } else {
            targetIndex = parent[nodeIndex];
            while (targetIndex != -1 && right[targetIndex] == nodeIndex) {
                nodeIndex = targetIndex;
                targetIndex = parent[targetIndex];
            }

            if (targetIndex != -1) {
                return targetIndex;
            }
        }

        return nodeIndex;
    }

    static int prev(int nodeIndex) {
        int targetIndex = left[nodeIndex];
        if (targetIndex != -1) {
            while (right[targetIndex] != -1) {
                targetIndex = right[targetIndex];
            }
            return targetIndex;

        } else {
            targetIndex = parent[nodeIndex];
            while (targetIndex != -1 && left[targetIndex] == nodeIndex) {
                nodeIndex = targetIndex;
                targetIndex = parent[targetIndex];
            }

            if (targetIndex != -1) {
                return targetIndex;
            }
        }

        return nodeIndex;
    }
}

/** Simple yet moderately fast I/O routines.
 *
 * Example usage:
 *
 * Kattio io = new Kattio(System.in, System.out);
 *
 * while (io.hasMoreTokens()) {
 *    int n = io.getInt();
 *    double d = io.getDouble();
 *    double ans = d*n;
 *
 *    io.println("Answer: " + ans);
 * }
 *
 * io.close();
 *
 *
 * Some notes:
 *
 * - When done, you should always do io.close() or io.flush() on the
 *   Kattio-instance, otherwise, you may lose output.
 *
 * - The getInt(), getDouble(), and getLong() methods will throw an
 *   exception if there is no more data in the input, so it is generally
 *   a good idea to use hasMoreTokens() to check for end-of-file.
 *
 * @author: Kattis
 */

class Kattio extends PrintWriter {
    public Kattio(InputStream i) {
        super(new BufferedOutputStream(System.out));
        r = new BufferedReader(new InputStreamReader(i));
    }
    public Kattio(InputStream i, OutputStream o) {
        super(new BufferedOutputStream(o));
        r = new BufferedReader(new InputStreamReader(i));
    }

    public boolean hasMoreTokens() {
        return peekToken() != null;
    }

    public int getInt() {
        return Integer.parseInt(nextToken());
    }

    public double getDouble() {
        return Double.parseDouble(nextToken());
    }

    public long getLong() {
        return Long.parseLong(nextToken());
    }

    public String getWord() {
        return nextToken();
    }

    private BufferedReader r;
    private String line;
    private StringTokenizer st;
    private String token;

    private String peekToken() {
        if (token == null)
            try {
                while (st == null || !st.hasMoreTokens()) {
                    line = r.readLine();
                    if (line == null) return null;
                    st = new StringTokenizer(line);
                }
                token = st.nextToken();
            } catch (IOException e) { }
        return token;
    }

    private String nextToken() {
        String ans = peekToken();
        token = null;
        return ans;
    }
}