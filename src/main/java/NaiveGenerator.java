import java.util.ArrayList;
import java.util.Random;

public class NaiveGenerator {
    private int mask = 0x7FFFFFFF;
    private Random random;

    public class Result {
        public int[] input;
        public long output;

        Result(int[] input, long output) {
            this.input = input;
            this.output = output;
        }
    }

    public NaiveGenerator (int mask) {
        this.mask = mask;
        random = new Random();
    }

    public Result produce(int size) {
        long sumOfMedians = 0;
        int[] values = random.ints(size).map(i -> i & mask).toArray();

        ArrayList<Integer> working = new ArrayList();
        for (int i : values) {
            working.add(i);
            int[] sorted = working.stream().mapToInt(val -> (int)val).sorted().toArray();

            int medianIdx = (int)(Math.ceil((double)sorted.length / 2) - 1);
            if (sorted.length % 2 == 1) {
                sumOfMedians += sorted[medianIdx];
                continue;
            }

            sumOfMedians += (sorted[medianIdx] + sorted[medianIdx + 1]) / 2;
        }

        return new Result(values, sumOfMedians);
    }
}
